#Uses k nearest neighbors to differentiate images of leaves from those of faces
#Much better designed than my January 2018 implementation
import numpy as np
from os import listdir
from sys import argv
from random import shuffle
from PIL import Image

#loads an image and converts it to three matrices, one each for R, G, and B
def image_matrix(filename):
    im = Image.open(filename)
    pix = im.load()
    mats = [np.matrix([[pix[i,j][k] for i in range(im.size[0])] for j in range(im.size[1])]) for k in range(3)]
    return mats

#set data and hyperparameter
if len(argv) < 2:
    print("Please enter a valid image dir")
    exit(1)

K = 3
if len(argv) == 3:
    K = int(argv[2])

repo = argv[1]

categories = repo.split("-") #repo should be named "class1-class2"
tt_ratio = 0.75 #train test ratio

#randomly split pictures into training and test sets
pics = listdir(repo)
np.random.shuffle(pics)
train = pics[:int(tt_ratio*len(pics))]
test = pics[int(tt_ratio*len(pics)):]

#Convert input images to matrices
train_vectors = [image_matrix(repo+"/"+image) for image in train]
test_vectors = [image_matrix(repo+"/"+image) for image in test]

#Iterate over pics in the test set
for j,pic in enumerate(test_vectors):
    #for each pic in the training set get the sum of the norms of the difference between it and the test image
    #also pair it with the training image's name for classification purposes
    distvec = [(sum([np.linalg.norm(pic[k]-tpic[k]) for k in range(3)]),train[i]) for i,tpic in enumerate(train_vectors)]

    #sort by distance, get K closest images
    nearest = sorted(distvec, key = lambda x:x[0])[:K]
    #find which category is represented more in the nearest images set, output said category
    for cat in categories:    
        if len(list(filter(lambda x:cat in x[1], nearest))) >= K/2:
            print("Classifying image ", test[j], " as ", cat)
