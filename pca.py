#An experiment in image compression using PCA
#Jakob Coles
#May 20, 2019
import numpy as np
import pickle, os
from PIL import Image
from sys import argv

#Reduces data to k dimensions
def pca(data, k):
    avg = np.mean(data,axis=1)
    sigma = (data-avg)*np.transpose(data-avg)/data.shape[1] #covariance matrix
    e, v = np.linalg.eig(sigma) #(e)igenvalues and eigen(v)ectors
    eigens = sorted([(item, v[:,i]) for i,item in enumerate(e)], key=lambda x:x[0], reverse=True)[:k] #sort by eigenvalue size
    topk = np.hstack(list(map(lambda x:x[1], eigens))) #matrix of k largest eigenvectors
    return (np.transpose(topk)*data, topk) #projection


#Attempts to reconstruct original image from data and eigenvectors
#https://stats.stackexchange.com/questions/229092/how-to-reverse-pca-and-reconstruct-original-variables-from-several-principal-com
def reconstruct(eigenvectors, data):
    return eigenvectors*data

#Converts image to three matrices of pixels (r,g,b values)
def image_matrix(filename):
    im = Image.open(filename)
    pix = im.load()
    mats = [np.matrix([[pix[i,j][k] for i in range(im.size[0])] for j in range(im.size[1])]) for k in range(3)]
    return mats

#Main
#sample run: python pca.py image.jpg 5,50,500 -savepkl
infile = argv[1] #image file name
comps = list(map(int, argv[2].split(","))) #list of k values for PCA
r, g, b = image_matrix(infile)

for num in comps:
    #get the projection for red, green, and blue
    proj_r, eigenvectors_r = pca(r, num)
    proj_g, eigenvectors_g = pca(g, num)
    proj_b, eigenvectors_b = pca(b, num)

    #attempt to reconstruct original image
    pr2 = reconstruct(eigenvectors_r, proj_r).tolist()
    pg2 = reconstruct(eigenvectors_g, proj_g).tolist()
    pb2 = reconstruct(eigenvectors_b, proj_b).tolist()

    #dump eigenvectors to pickle file so we can see the file sizes
    with open(infile+"_pca_"+num, "r") as pklfile:
        pickle.dump((proj_r, proj_g, proj_b, eigenvectors_r, eigenvectors_g, eigenvectors_b), pklfile)
        print("File size with %d components: %d bytes"%d(num, os.path.getsize(pklfile.name)))
        #unless you want to keep the pickle files, they get removed to save space
        if len(argv) < 4 or argv[3]!="-savepkl":
            os.remove(pklfile.name)
    
    #turn reconstruction into np array for use by PIL
    temp = np.array([[[int(pr2[i][j].real)%256, int(pg2[i][j].real)%256, int(pb2[i][j].real)%256] for j in range(len(pr2[0]))] for i in range(len(pr2))]).astype("uint8")

    #write reconstructed image to file so we can examine it
    image = Image.fromarray(temp, "RGB")
    image.save("%s_%d_component.jpg"%(infile,num))
    print("Reduced with %d components"%num)
