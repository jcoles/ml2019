#Linear regression implemented from scratch
import numpy as np
import pickle
from sklearn.model_selection import train_test_split
from sys import argv

#A linear model
class Linreg:

    def __init__(self, n):
        self.w = np.zeros((n,1)) #n is the number of variables
        self.b = 0

    #returns z
    def linear(self, X):
        return self.w.T*X+self.b

    #mean squared error loss
    def loss(self, z, y):
        return np.linalg.norm(z-y)**2*(1/z.shape[1])

    #gradient of w
    def dw(self, X, z, y):
        return (X*(z-y).T)*(1/z.shape[1])

    #gradient of b
    def db(self, z, y):
        return np.sum(z-y)

    #performs gradient descent on given training set. alpha=learning rate.
    def train(self, train_X, train_y, alpha, iters):
        for i in range(iters):
            z = self.linear(train_X)
            print("Loss at iter %d: "%(i+1), self.loss(z, train_y))
            dw_ = self.dw(train_X, z, train_y)
            db_ = self.db(z, train_y)
            self.w -= alpha*dw_
            self.b -= alpha*db_
            

data_file = argv[1] #a pickle file
num_iters = int(argv[2])

with open(data_file, "rb") as data:
    X, y = pickle.load(data)

#The dimensionality of these are wrong so we have to transpose them again to feed them into the model
X_train, X_test, y_train, y_test = train_test_split(X.T, y.T)

model = Linreg(X_train.T.shape[0])
model.train(X_train.T, y_train.T, 0.01/X_test.shape[0], num_iters)
print("Found parameters w: ", model.w.tolist(), "; b: ", model.b)

#test the model
result = model.linear(X_test.T)
print("Loss on test set: ", model.loss(result, y_test.T))
