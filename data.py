#A script to generate data according to a linear model, with some noise thrown in
#sample command: python data.py 100 200,200 -100,-100 1,1 0 2.4 data.pkl
#Jakob Coles
#May 19, 2019

import pickle
from sys import argv
from random import gauss, random
import numpy as np
sample_size = int(argv[1]) #number of samples
sample_range = list(map(float, argv[2].split(","))) #range for each variable
sample_min = list(map(float, argv[3].split(","))) #lower bound for each variable
slope = list(map(float, argv[4].split(","))) #coefficient for each variable in the linear model
bias = float(argv[5]) #bias of the model
stddev = float(argv[6]) #standard deviation of random noise
outfile = argv[7] #destination file (pickle)

var_count = len(slope) #number of varibles

#randomly generate X values within the given bounds
data_X = np.matrix([[random()*sample_range[i]+sample_min[i] for i in range(var_count)] for j in range(sample_size)])

#calculate y for each of the above X in accordance with the model with random noise added on top
data_y = np.matrix([sum([slope[i]*data_X[j,i] for i in range(var_count)])+bias+gauss(0,stddev) for j in range(sample_size)])

#output
with open(outfile, "wb+") as pkl:
    pickle.dump((data_X.T,data_y), pkl)
